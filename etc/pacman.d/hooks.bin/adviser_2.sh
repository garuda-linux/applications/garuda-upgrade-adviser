#!/usr/bin/env bash
# cat /etc/pacman.d/hooks.bin/adviser_2.sh
#
# enable script debug mode (uncomment the line below):
# set -xv
#
uid="$(find /run/user/ -maxdepth 1 -mindepth 1 -type d | awk -F/ '{print $NF}')"
user="$(grep $uid /etc/passwd  | sed 's/:.*//')" 
display="$(DISPLAY=$(echo "$DISPLAY"))"
xdg_rd="$(XDG_RUNTIME_DIR=/run/user/$uid)"
xauth="$(XAUTHORITY=/home/$(grep $uid /etc/passwd  | sed 's/:.*//')/.Xauthority)"
dbus_sba="$(DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$uid/bus)"
    export XAUTHORITY=/home/$user/.Xauthority
    export DISPLAY=$(echo "$DISPLAY")
    export DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/$uid/bus"
su $user -c  "$display $xdg_rd $xauth $dbus_sba /usr/bin/notify-send 'An upgrade has been initiated which includes important core components.'" >/dev/null 2>&1
    # create the timestamp file /home/$USER/.local/cache/major_update.log to identify when a major update was begun.
sudo -Hiu $user sh -c "/usr/bin/echo -e "$(date +%s)" > /home/$user/.local/cache/major_update.log" >/dev/null 2>&1
    # issue a popup notification when a major update that likely requires a reboot has begun.
su $user -c  "$display $xdg_rd $xauth $dbus_sba /usr/bin/notify-send --urgency=critical 'This update will likely require a reboot after its completion.'" >/dev/null 2>&1