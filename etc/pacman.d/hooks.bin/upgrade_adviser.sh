#!/usr/bin/env bash
# cat /etc/pacman.d/hooks.bin/upgrade_adviser.sh
# enable script debug mode (uncomment the line below):
# set -x
uid="$(find /run/user/ -maxdepth 1 -mindepth 1 -type d | awk -F/ '{print $NF}')"
user="$(grep $uid /etc/passwd  | sed 's/:.*//')" 
display="$(DISPLAY=$(echo "$DISPLAY"))"
xdg_rd="$(XDG_RUNTIME_DIR=/run/user/$uid)"
xauth="$(XAUTHORITY=/home/$(grep $uid /etc/passwd  | sed 's/:.*//')/.Xauthority)"
dbus_sba="$(DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$uid/bus)"
pacfiles="$(pacdiff -o)"
orphans="$(pacman -Qtd | awk '{ print $1 }')"
aur="$(pacman -Qmq)"
aur_check="$(pamac checkupdates -a | grep AUR)"
    # color codes
black='\033[0;30m'
dk_gray='\033[1;30m'
red='\033[0;31m'
lt_red='\033[1;31m'
green='\033[0;32m'  
lt_green='\033[1;32m'
yellow='\033[1;33m'
blue='\033[0;34m'
lt_blue='\033[1;34m'
purple='\033[0;35m'
lt_purple='\033[1;35m'
orange='\033[0;33m'
cyan='\033[0;36m' 
lt_gray='\033[0;37m'    
white='\033[1;37m'
default='\033[0m' # Default Color
    export XAUTHORITY=/home/$user/.Xauthority
    export DISPLAY=$(echo "$DISPLAY")
    export DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/$uid/bus"
# change the ownership of /tmp/pac_files.log.
    chown $user: /tmp/pac_files.log >/dev/null 2>&1
    # cancel if run in a tty
case $(tty) in /dev/tty[0-9]*)
    exit 0 ;;
esac
    # delete /tmp/orphans.log to prevent errors occuring later in the script.
if  [ -f /tmp/orphans.log ]; then
    rm  /tmp/orphans.log >/dev/null 2>&1
fi
    # disable redundant Garuda hooks that are no longer required.
    # if /etc/pacman.d/hooks/000_pacman-sync.hook exists, then remove it.
if  [ -f /etc/pacman.d/hooks/000_pacman-sync.hook ]; then
    rm /etc/pacman.d/hooks/000_pacman-sync.hook >/dev/null 2>&1
fi
# if /etc/pacman.d/hooks/pkgfile.hook exists, then remove it.
if  [ -f /etc/pacman.d/hooks/pkgfile.hook ]; then
    rm /etc/pacman.d/hooks/pkgfile.hook >/dev/null 2>&1
fi
# if /etc/pacman.d/hooks/pacnew-check.hook exists, then remove it.
if  [ -f /etc/pacman.d/hooks/pacnew-check.hook ]; then
    rm /etc/pacman.d/hooks/pacnew-check.hook >/dev/null 2>&1
fi
# if /etc/pacman.d/hooks/orphans.hook exists, then remove it.
if  [ -f /etc/pacman.d/hooks/orphans.hook ]; then
    rm /etc/pacman.d/hooks/orphans.hook >/dev/null 2>&1
fi
# if /etc/pacman.d/hooks/foreign.hook exists, then remove it.
if  [ -f /etc/pacman.d/hooks/foreign.hook ]; then
    rm /etc/pacman.d/hooks/foreign.hook >/dev/null 2>&1
fi
# if /etc/pacman.d/hooks/just-reboot.hook exists, then remove it.
if  [ -f /etc/pacman.d/hooks/just-reboot.hook ]; then
    chattr -i /etc/pacman.d/hooks/just-reboot.hook >/dev/null 2>&1
    rm /etc/pacman.d/hooks/just-reboot.hook >/dev/null 2>&1
fi
    echo -e "\n${blue}* * *     * * *     * * *     * * *     * * *     * * *${default}\n"            # blue colored output divider
    echo -e "${yellow}Return a list of all orphaned packages detected in the system ...${default}\n"     # yellow colored textual output
    echo -e "${orange}$orphans:${default}\n"        # orange colored output, (package version numbers stripped).
if [ $? -eq 0 ]; then
    # copy a list of any orphaned packages detected in the system to /tmp/orpans.log
    echo -e "$orphans" | sudo -Hiu $user tee /tmp/orphans.log >/dev/null 2>&1  
    echo -e "${yellow}Double check package dependencies before uninstalling ...\n${default}"      # yellow colored textual output
else
    echo -e "${lt_green}No orphaned packages detected in the system ...\n${default}"       # light green colored textual output
fi
    echo -e "${blue}* * *     * * *     * * *     * * *     * * *     * * *${default}"            # blue colored output divider
    echo -e "\n${green}$aur${default}"  
if [ $? -eq 0 ]; then
    echo -e "\n${lt_green}The AUR packages listed above are currently installed on the system ...\n${default}"      # green colored textual output
else
    echo -e "${lt_green}No foreign packages have been detected in the system ...${default}"      # light green colored textual output
fi
# WORKING #    echo -e "${green}$aur_check${default}"
    
if [[ $(nmcli -f STATE -t g) != 'connected' ]]; then 
echo -e "${green}Checking for outdated installed AUR packages ...${default}\n" ; fi && echo -e "${green}$aur_check${default}"
if [ $? -eq 0 ]; then
    echo -e "\n${lt_green}The AUR packages listed above are out of date and require updating ...${default}\n"      # light green colored textual output
fi
    echo -e "${blue}* * *     * * *     * * *     * * *     * * *     * * *${default}\n"            # blue colored output divider
    echo -e "${red}$pacfiles${default}\n"
if [ "$(cat /tmp/pac_files.log)" == "$(grep -Ei "pacnew|pacsave" /tmp/pac_files.log)" ]; then 
    echo -e "${red}Any pac* files listed above require attention...${default}\n"      # red colored textual output
    su $user -c  "$display $xdg_rd $xauth $dbus_sba /usr/bin/notify-send --urgency=critical -i /usr/share/icons/breeze/emblems/24/emblem-warning.svg 'Pac* files have been detected. Most pacnew files should be merged. On rare occaisions merging must be performd immediately'" >/dev/null 2>&1
    echo -e "${red}Most, but not all pacnew files require merging ...${default}\n"      # red colored textual output
    echo -e "${red}Very rarely pacnew files may require merging immediately ... ${default}\n"     # red colored textual output
    echo -e "${red}Information regarding merging pacnew files can be found at the following links:${default}\n"      # red colored textual output 
    # the websites below can be replaced with Garuda specific content once it has been created.
    echo -e "${purple}https://wiki.archlinux.org/index.php/Pacman/Pacnew_and_Pacsave${default}\n"       #hyperlinks in purple
    echo -e "${purple}https://wiki.archlinux.org/index.php/System_maintenance#Deal_promptly_with_new_configuration_files${default}\n"
    # The websites below can be replaced with Garuda specific content once it has been created.
    su $user -c "xdg-open https://wiki.archlinux.org/index.php/Pacman/Pacnew_and_Pacsave &> /dev/null & disown"
    # su $user -c "xdg-open https://wiki.archlinux.org/index.php/System_maintenance#Deal_promptly_with_new_configuration_files &> /dev/null &
else
   echo -e "${lt_green}No pacnew or pacsave files found in the system ...${default}\n"       # light green colored textual output
fi
    echo -e "${blue}* * *     * * *     * * *     * * *     * * *     * * *${default}\n"            # blue colored output divider
    # Create a Unix timestamp file to identify when the update was completed.
sudo -Hiu $user sh -c "/usr/bin/echo -e "$(date +%s)" > /home/$user/.local/cache/last_upgrade.log" >/dev/null 2>&1