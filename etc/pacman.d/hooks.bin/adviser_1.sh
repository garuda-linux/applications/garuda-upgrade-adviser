#!/usr/bin/env bash
#cat /etc/pacman.d/hooks.bin/adviser_1.sh
#
# enable script debug mode (uncomment the line below):
#set -xv
#
uid="$(find /run/user/ -maxdepth 1 -mindepth 1 -type d | awk -F/ '{print $NF}')"
user="$(grep $uid /etc/passwd  | sed 's/:.*//')" 
display="$(DISPLAY=$(echo "$DISPLAY"))"
xdg_rd="$(XDG_RUNTIME_DIR=/run/user/$uid)"
xauth="$(XAUTHORITY=/home/$(grep $uid /etc/passwd  | sed 's/:.*//')/.Xauthority)"
dbus_sba="$(DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$uid/bus)"
export XAUTHORITY=/home/$user/.Xauthority
export DISPLAY=$(echo "$DISPLAY")
export DBUS_SESSION_BUS_ADDRESS="unix:path=/run/user/$uid/bus"
    # anounce that an update is underway, (commented normally, this is useful when troubleshooting).
    # su $user -c  "$display $xdg_rd $xauth $dbus_sba /usr/bin/notify-send 'A new installation and/or upgrade has been initiated.'" >/dev/null 2>&1
    # create the timestamp file "/home/$USER/.local/cache/minor_update.log" to identify when a minor update has begun.
sudo -Hiu $user sh -c "/usr/bin/echo -e "$(date +%s)" > /home/$user/.local/cache/minor_update.log"
# remove /tmp/pac_files.log if the file exists.
if  [ -f /tmp/pac_files.log ]; then
    rm /tmp/pac_files.log
fi
# remove /home/$USER/.local/cache/major_update.log if the file exists.
if  [ -f /home/$user/.local/cache/major_update.log ]; then
    rm  /home/$user/.local/cache/major_update.log
fi
    su $user -c  "$display $xdg_rd $xauth $dbus_sba systemctl --user status adviser.service | grep -Ei 'dead|refusing|stderr'"
if [ $? -eq 0 ]; then
    echo  -e  Error: Garuda Adviser Service is not operational $(date +%Y-%m-%d@%H:%M) | sudo -Hiu $user tee -a /tmp/adviser_stderr.log >/dev/null 2>&1
    su $user -c  "$display $xdg_rd $xauth $dbus_sba /usr/bin/notify-send 'Error:  The Garuda Update Adviser service is not operational ...'" >/dev/null 2>&1
    sleep 5
    su $user -c  "$display $xdg_rd $xauth $dbus_sba /usr/bin/notify-send 'Disabling the Adviser service is not recommended ...'" >/dev/null 2>&1
    sleep 5
    su $user -c  "$display $xdg_rd $xauth $dbus_sba /usr/bin/notify-send 'An attempt will be made to restart the Garuda Adviser service now  ...'" >/dev/null 2>&1
    # Attempt to restart the Garuda Adviser Service, and notify if successful.
    sleep 5
    su $user -c  "$display $xdg_rd $xauth $dbus_sba /usr/bin/systemctl --user enable --now adviser.service" 
if [ $? -eq 0 ]; then  
      su $user -c  "$display $xdg_rd $xauth $dbus_sba /usr/bin/notify-send  'The Garuda Adviser Service is now operational '" >/dev/null 2>&1  
else
    echo  -e  Error: Garuda Adviser Service failed to start correctly, and the service was unable to be restarted $(date +%Y-%m-%d@%H:%M) | sudo -Hiu $user tee -a /tmp/adviser_stderr.log >/dev/null 2>&1
    su $user -c  "$display $xdg_rd $xauth $dbus_sba /usr/bin/notify-send --urgency=critical -i /usr/share/icons/breeze/emblems/24/emblem-warning.svg 'Error: Garuda Adviser Service failed to start correctly - the service was unable to be restarted ...'"  >/dev/null 2>&1
fi
fi