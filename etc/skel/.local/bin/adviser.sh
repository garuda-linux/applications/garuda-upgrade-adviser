#!/usr/bin/env bash
#cat //home/$USER//.local/bin/adviser.sh
# enable script debug mode (uncomment the line below):
# set -xv
#
# Create  the required service storage directories if not in existence.
# mkdir -p /home/$USER/.local/bin
# mkdir -p /home/$USER/.local/cache
# You may choose alternate locations if you wish. 
# The paths in the script must be altered if the storage locations are altered.
#
# Check if the script is being executed as root and exit if run as root.
if [ "$EUID" -eq 0 ]; then
  echo "This script cannot be executed as root - exiting script"
  sleep 2
  exit 1
fi
    # Check if the script is being executed in a tty and exit if running in a tty.
case $(tty) in /dev/tty[0-9]*)
    exit 0 ;;
esac
    # Check if system updates are being performed on a regular basis.
    day=$((($(date +%s) - $(date -d $(sed -n '/upgrade$/x;${x;s/.\([0-9-]*\).*/\1/p}' /var/log/pacman.log) +%s)) / 86400))
printf '%d\n' 
    # Example below : [[ $? -ge 14 ]] the number assigned inside the brackets is the number of days to trigger a warning.
if [[ $? -ge 14 ]]; then
    # Issue a warning if system updates are not being performed on a regular basis.
    notify-send --urgency=critical  -i /usr/share/icons/breeze/emblems/24/emblem-warning.svg 'Rolling distributions require regular updates.  Updating your system is overdue and needs to be performed ...' >/dev/null 2>&1
fi
    # if /home/$USER/.local/cache/minor_update.log exists, then delete it.
if  [ -f /home/$USER/.local/cache/minor_update.log ]; then
    rm /home/$USER/.local/cache/minor_update.log || echo  -e "Error: failed to delete  /home/$USER/.local/cache/minor_update.log $(date +%Y-%m-%d@%H:%M)" | tee -a /tmp/adviser_stderr.log
fi
    # if /tmp/orphans.log exists, then rename it to /tmp/orphans_log.old.
if  [ -f /tmp/orphans.log ]; then
    mv /tmp/orphans.log /tmp/orphans_log.old
    # The popup announcement below is useful when troubleshooting, (normally commented).
    #notify-send  '/tmp/orphans.log renamed to ... /tmp/orphans_log.old'
fi
    # if tmp/pac_files.log exists, then rename it to tmp/pac_files.log.old.
if  [ -f /tmp/pac_files.log ]; then
    mv /tmp/pac_files.log /tmp/pac_files_log.old
    # The popup announcement below is useful when troubleshooting, (normally commented).
    #notify-send  '/tmp/pac_files.log renamed to ... /tmp/pac_files.log.old'
fi
    notify-send  'Garuda Adviser Service  - Now in passive monitor mode  - Scanning for new update initiation' >/dev/null 2>&1
    # This checks in a continuous loop until a timestamp file indicating an update has been initiated is present.
until  [ -f /home/$USER/.local/cache/minor_update.log ]
do 
    echo "Garuda Adviser Service  - Now in passive monitor mode  - Scanning for new update initiation ..."  
    sleep 5; 
done
    echo -e "A new installation and/or upgrade was initiated." 
    # The popup announcement below is useful when troubleshooting, (normally commented).
    # notify-send 'A new installation and/or upgrade was initiated.' 
    # check for internet connectivity, and notify if a connection was not possible.
    # uncomment to ping Google's servers to determine if a connection is present (server 8.8.8.8)
    # ping -c 1 8.8.8.8  | grep received
    # ping Quad9 secure DNS servers to determine if a connection is present (server 9.9.9.9)
    # see: https://www.techradar.com/reviews/quad9-dns
ping -c 1 9.9.9.9  | grep received
     if [ $? -eq 0 ]; then 
     echo -e "There is an Internet connection present to perform system updates" 
else
    echo -e "Error: Internet connection broken $(date +%Y-%m-%d@%H:%M)" | tee -a /tmp/adviser_stderr.log
    notify-send  'No internet connection was detected - ensure a connection is present before initiating an update ...' >/dev/null 2>&1
    # exit the script if no internet connection is available.
    exit 
fi
    # This deletes the /home/$USER//.local/cache/minor_update.log after the timestamp file has been detected.
if  [ -f /home/$USER/.local/cache/minor_update.log ]; then
    rm /home/$USER/.local/cache/minor_update.log || echo  -e "Error: failed to delete  /home/$USER/.local/cache/minor_update.log $(date +%Y-%m-%d@%H:%M)" | tee -a /tmp/adviser_stderr.log
fi
    # This downloads the full Arch rss feed and saves it as: /tmp/arch_feed.html
    wget --quiet --convert-links --output-document /tmp/arch_feed.html https://www.archlinux.org/feeds/news/ || echo 'Error: Arch  RSS feed download unsucessful $(date +%Y-%m-%d@%H:%M)'  | tee -a /tmp/adviser_stderr.log
    # This strips the full rss feed down to only the announcement dates. 
xmlgetnext () {
   local IFS='>'
   read -d '<' TAG VALUE
}
cat /tmp/arch_feed.html | while xmlgetnext ; do
   case $TAG in
      'item')
         pubDate=''
         ;;
      'pubDate')
         pubDate="$VALUE"
         ;;
      '/item')
         cat<<EOF
$pubDate
EOF
         ;;
      esac
    # the calender date of the most recent announcement is saved to /home/$USER/.local/cache/feed.log     
done | head -n 1 | cut -d ' ' -f 2-6 > /home/$USER/.local/cache/feed.log
    # This converts the announcement release time/date format from the calender format to the Unix epoch format.
        calender="$(cat /home/$USER/.local/cache/feed.log)"
        epoch=$(date -d "${calender}" +"%s")   
    echo "$epoch" > /home/$USER/.local/cache/last_announce.log
    # /home/$USER/.local/cache/last_upgrade.log timestamp compared against /home/$USER/.local/cache/last_announce.log timestamp
        announce_date="$(</home/$USER/.local/cache/last_announce.log)"
        upgrade_date="$(</home/$USER/.local/cache/last_upgrade.log)"
    # Comment out the line above to simulate a newly released Arch announcement.
    # Uncomment the line below to simulate a newly released Arch announcement.
    # upgrade_date="$(date --date="90 days ago"  +"%s")"
    # save the above changes, but be sure to reverse the changes afterwards.
    #This compares the numerical difference between the rss announcement timestamp and the last update timestamp.
if [ "$announce_date" -lt "$upgrade_date" ]; then
    # If the timestamp from last announcement date is less than the last upgrade timestamp then no new announcements have been released ... 
    #notify-send 'No new update announcements have been released' >/dev/null 2>&1
    echo 'No new update announcements have been released since the last update ocurred'
else
    # There have been new Arch update announcement(s) released since the last system update was performed.
        echo "New update announcements have been released since the last update ocurred"
    # play an audible alert to announce a new update release, (if the sound file isn't present, the error will be hidden). 
    # paplay /usr/share/sounds/freedesktop/stereo/alarm-clock-elapsed.oga
        paplay /usr/share/sounds/freedesktop/stereo/alarm-clock-elapsed.oga  > 2>&1 /tmp/adviser_stderr.log 
    # Display a popup message informing the user that new update announcement(s) have been released.
        notify-send --urgency=critical  'New update announcement(s) have been released, manual interventions may be required.'  >/dev/null 2>&1
    # The Arch homepage will now be opened to display new update announcements.    
        notify-send 'The Arch homepage will now be opened to display new update announcements.' >/dev/null 2>&1 
    xdg-open https://www.archlinux.org/ 
fi
    # This checks for the creation of a file indicating a major update has been initiated.
    # If it exists, then it is deleted and a series of pop up messages are initiated informing the user to reboot.
    # check if the /home/$USER/.local/cache/major_update.log exists.
if  [ -f /home/$user/.local/cache/major_update.log ]; then
    # remove /home/$USER/.local/cache/major_update.log if the file exists.
    rm  /home/$user/.local/cache/major_update.log
   notify-send --urgency=critical   -i /usr/share/icons/breeze/emblems/24/emblem-warning.svg 'A reboot will likely be required after this update.' >/dev/null 2>&1
    sleep 15
    notify-send 'Major update in progress...' 'You may want to postpone this update if you are unable to reboot at this time' >/dev/null 2>&1 
    sleep 15
    notify-send 'Always perform a reboot if you experience any unusual issues after any update' >/dev/null 2>&1 
    sleep 15
else
    #  The script is put into a loop until the file detection signifies the update is completed.
until [ -f /tmp/orphans.log -a -f /tmp/pac_files.log ]
 do 
    echo "Garuda Adviser Service  - Waiting for update completion ..." 
    sleep 5; 
 done
fi
    # copy /tmp/orphans.log to ~/.local/cache/orphans.log
if  [ -f /tmp/orphans.log ]; then
    cp /tmp/orphans.log /home/$USER/.local/cache/orphans.log >/dev/null 2>&1
fi
    # copy /tmp/pac_files.log to ~/.local/cache/pac_files.log
if  [ -f /tmp/pac_files.log ]; then
    cp /tmp/pac_files.log /home/$USER/.local/cache/pac_files.log  >/dev/null 2>&1
fi
    # Checks if the lists of orphaned and pac* files have been created.
if  [ -f /home/$USER/.local/cache/orphans.log -a -f /home/$USER/.local/cache/pac_files.log ]; then
    echo "Lists of orphaned and pac* files have been copied to the ~/.local/cache/ directory ..." 
    #notify-send 'Lists of orphaned and pac* files have been copied to the ~/.local/cache/ directory ...' >/dev/null 2>&1   
else
    echo  -e  "Error: failed to create lists of orphaned and pac* files $(date +%Y-%m-%d@%H:%M)" | tee -a /tmp/adviser_stderr.log
fi
    # create backup lists of all native and foreign packages.
        pacman -Qqen  > /home/$USER/.local/cache/native_packages.log
        pacman -Qqem > /home/$USER/.local/cache/foreign_packages.log
    # Checks if the backup lists of native and foreign packages have been created.
if  [ -f /home/$USER/.local/cache/native_packages.log -a -f /home/$USER/.local/cache/foreign_packages.log ]; then
    #notify-send 'Lists of all installed native and foreign packages have been backed up to ~/.local/cache' >/dev/null 2>&1
    echo -e  'Lists of all installed native and foreign packages have been backed up to ~/.local/cache' 
else
    echo  -e "Error: failed to create backup installed package lists $(date +%Y-%m-%d@%H:%M)"  | tee -a /tmp/adviser_stderr.log
fi
# Print a list of the most recent installation errors/warnings to /home/$USER/.local/cache/pac_err.log 
    echo -e "\n--------------------------------------------------------------------------------------------------------------------------------------" > /home/$USER/.local/cache/pac_err.log 
    echo -e "This is a filtered print out of the most recent installation errors/warnings of any significance:" >> /home/$USER/.local/cache/pac_err.log 
    echo -e "--------------------------------------------------------------------------------------------------------------------------------------\n" >> /home/$USER/.local/cache/pac_err.log 
    echo -e "\n--------------------------------------------------------------------------------------------------------------------------------------\n"
    echo -e "Any pacnew warnings received were written to /home/$USER/.local/cache/pac_err.log " >> /home/$USER/.local/cache/pac_err.log 
    echo -e "\nThe full pacman installation log is located at /var/log/pacman.log\n" >> /home/$USER/.local/cache/pac_err.log 
    echo -e "--------------------------------------------------------------------------------------------------------------------------------------\n"  >> /home/$USER/.local/cache/pac_err.log 
        grep -Ei 'error|warning'  /var/log/pacman.log | grep -viE 'Possibly|pacnew|pacsave' | tail -n 20 | sed G  >> /home/$USER/.local/cache/pac_err.log  
    echo -e "\n--------------------------------------------------------------------------------------------------------------------------------------\n"
    sleep 20
    notify-send 'The Adviser Service has completed its active upgrade tasks and will now return to passive monitor mode' >/dev/null 2>&1 
    sleep 30
    notify-send 'The update process may still be underway for some time if the kernel(s) require updating' >/dev/null 2>&1 
# run the script continuously.
exec $0